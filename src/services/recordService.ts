import axios from "axios";
import { AxiosResponse } from "axios";

export type Record = {
	id: string;
	content: string;
}

export class RecordService {

	public async retrieveList(): Promise<Record[]> {
		const res: AxiosResponse = await axios.get("/records");

		return res.data.records.reverse();
	}

	public async add(content: string): Promise<Record> {
		const res: AxiosResponse = await axios.put("/record", { content });

		if(res.status != 200) throw res;

		return res.data;
	}

}

export const recordService: RecordService = new RecordService();
