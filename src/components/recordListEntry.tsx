import * as React from "react";

type Props = {
	record: { id: string, content: string };
};

type State = {};

export class RecordListEntry extends React.Component<Props, State> {

	constructor(props: Props) {
		super(props);
	}

	render = () => (
		<div className="record-list-entry">
			<header>
				{ this.props.record.id }
			</header>
			<main>
				{ this.props.record.content }
			</main>
		</div>
	);

}
