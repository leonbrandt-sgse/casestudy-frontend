import * as React from "react";
import { RecordListEntry } from "./recordListEntry";

import { recordService } from "../services/recordService";

type Props = {};
type State = {
	records: any[];
};

export class RecordList extends React.Component<Props, State> {

	constructor(props: Props) {
		super(props);

		this.state = {
			records: [],
		};
	}

	fetch = async () => {
		const records = await recordService.retrieveList();
		this.setState({ records });
	}

	render = () => (
		<div className="record-list">
			<header>
				<button
					onClick={ this.fetch }
				>
					FETCH
				</button>
			</header>

			<main>
				{
					this.state.records.map((r, i) => <RecordListEntry record={r} />)
				}
			</main>
		</div>
	);

}
