import * as React from "react";

import { recordService } from "../services/recordService";

type Props = {};
type State = {
	content: string;
};

export class RecordAddForm extends React.Component<Props, State> {
	
	constructor(props: Props) {
		super(props);

		this.state = {
			content: "",
		};
	}

	add = async () => {
		await recordService.add(this.state.content);
	}

	handleKeyDown = (e: any) => {
		if(e.key === "Enter") {
			this.add();
		}
	}

	render = () => (
		<div className="record-add-form">
			<input
				value={ this.state.content }
				onChange={ e => this.setState({ content: e.target.value }) }
				onKeyDown={ this.handleKeyDown }
			/>
			<button
				onClick={ this.add }
			>
				ADD
			</button>
		</div>
	);

}
