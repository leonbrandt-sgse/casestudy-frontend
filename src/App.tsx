import React from "react";
import "./style/css/style.css";

import { RecordAddForm } from "./components/recordAddForm";
import { RecordList } from "./components/recordList";

function App() {
	return (
		<div id="wrapper">
			<main>
				<RecordAddForm />
				<RecordList />
			</main>
		</div>
	);
}

export default App;
